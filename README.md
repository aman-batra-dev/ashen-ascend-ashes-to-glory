# **Ashen Ascend - Ashes to Glory**

Welcome to *Ashen Ascend*, an experimental action-adventure prototype inspired by the *Souls* series, developed as a research project since August 2023. The project explores the feasibility of integrating Neural Networks for adaptive enemy AI in real-time gameplay while tackling the computational challenges involved.  

## **Gameplay Preview**

[![Click to Play](https://img.youtube.com/vi/enYJhhBgAaI/0.jpg)](https://www.youtube.com/watch?v=enYJhhBgAaI)  

## **Project Overview**  

*Ashen Ascend* is a small-scale prototype designed to investigate how reinforcement learning techniques, specifically Deep Q Networks (DQN), could influence enemy behavior in an action-combat setting. While initial implementations successfully created an AI model, performance constraints prevented the AI from being fully integrated into real-time enemy encounters.

Despite these limitations, the project includes a functioning combat system with fundamental gameplay elements such as movement, stamina-based attacks, and a boss enemy with predefined behaviors.

## **Key Features**  

- **Experimental AI Integration** – Implemented a Neural Network using DQN in Unreal Engine 5 to explore adaptive AI behavior.  
- **Souls-like Combat Mechanics** – Player movement, hitbox-based attacks, stamina-based combat, and a boss encounter.  
- **Performance Analysis** – Evaluated the feasibility of real-time deep learning in a modern game engine and identified computational limitations.  
- **Developed in Unreal Engine 5** – Built using C++17, Behavior Trees, and Unreal Motion Graphics (UMG) for UI.  

## **Technologies Used**  

- **Programming Languages** – C++17 for core mechanics, Unreal Engine 5 for development.  
- **AI & Gameplay Systems** – Deep Q Networks (DQN) (partially integrated), Behavior Trees for enemy logic.  
- **Development Tools** – Unreal Motion Graphics (UMG) for UI, Blueprints for rapid iteration, and Git for version control.  

## **Installation**  

To explore *Ashen Ascend*, follow these steps:  

1. Install [Git](https://git-scm.com/) and [Git LFS](https://git-lfs.github.com/).  

2. Clone the repository:  
   ```sh
   git clone https://gitlab.com/aman-batra-dev/ashen-ascend-ashes-to-glory.git
   cd ashen-ascend-ashes-to-glory
   git lfs install
   git lfs pull
   ```

3. Run hg.exe

4. Install Unreal prerequisites

5. Voila! Play the game


## Contact

For questions or collaboration opportunities, please reach out via [email](mailto:amanbatra22002@gmail.com) or [GitLab Issues](https://gitlab.com/aman-batra-dev/ashen-ascend-ashes-to-glory/-/issues).

## Credits
Made by: 
[Aman Batra](https://gitlab.com/aman-batra-dev)